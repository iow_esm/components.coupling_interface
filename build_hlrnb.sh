#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module load intel/2024.2
module load impi/2021.13
module load netcdf/intel/4.9.2

FC=mpiifort
AR="ar"
IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# include paths
export IOW_ESM_NETCDF_INCLUDE=`env | grep ^CPATH= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf"){print $i}}}'`
export IOW_ESM_NETCDF_LIBRARY=`env | grep ^LIBRARY_PATH= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf"){print $i}}}'`

if [ $debug == "debug" ]; then
	FFLAGS="-O0 -r8 -fp-model precise -xHost -g -traceback -check all -DIOW_ESM_DEBUG"
	configuration="DEBUG"
else
	FFLAGS="-O3 -r8 -no-prec-div -fp-model fast -xCORE-AVX512 "
	configuration="PRODUCTION"
fi

source ./compile.sh


