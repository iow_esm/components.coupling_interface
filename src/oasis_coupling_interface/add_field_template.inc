! define concatenation macros (cannot be nested, thus for each length needed there is one: stupid)
#define join_(a, b) a ## b
#define join(a, b) join_(a,b)

#define join3_(a, b, c) a ## b ## c 
#define join3(a, b, c) join3_(a, b, c)

#define join4_(a, b, c, d) a ## b ## c ## d
#define join4(a, b, c, d) join4_(a, b, c, d)

! construct needed names from the given direction (DIR = out, in) and dimension (DIM = 1d, 2d; SH = ":", ":,:") 
#define FUNCTION_NAME join4(add_, DIR, put_field_, DIM)
#define PROCESS_TEMPLATE join(process_template_, DIM)
#define FIELDS join(DIR, put_fields)
#define NFIELDS join3(n_, DIR, put_fields)
#define OASIS_DIR join(OASIS_, DIR)

function FUNCTION_NAME(ci, grid_index, name, data, process, arguments) result(success)
    logical :: success

    class(oasis_coupling_interface) :: ci
    integer :: grid_index
    character(*), intent(in) :: name
    real, dimension(SH), intent(in) :: data
    procedure(PROCESS_TEMPLATE), optional :: process
    class(flexible_arguments), optional :: arguments

    integer :: n, ierror

    integer, dimension(:), allocatable :: var_actual_shape
    integer, dimension(2) :: var_nodims

    if (.not. ci%coupling_interface%FUNCTION_NAME(grid_index, name, data, process, arguments)) then
        success = .false.
        call ci%handle_error("Cannot add field to grid.", __FILE__, __LINE__)
        return
    endif

    ! construct the actual shape for OASIS
    allocate(var_actual_shape(2*size(ci%grids(grid_index)%shape)))

    ! first dimension
    var_actual_shape(1) = 1 ! minimum is always 1
    var_actual_shape(2) = ci%grids(grid_index)%shape(1) ! extend of first dimension

    ! if we have a 2d field
    if (size(ci%grids(grid_index)%shape) > 1) then
        var_actual_shape(3) = 1
        var_actual_shape(4) = ci%grids(grid_index)%shape(2) ! extend of second dimension
    endif

    var_nodims(1) = size(ci%grids(grid_index)%shape)
    var_nodims(2) = 1

    call ci%log%write("Define OASIS field", LOG_INFO)

    ! do OASIS psecific stuff here
    n = ci%grids(grid_index)%NFIELDS
    
    CALL oasis_def_var( ci%grids(grid_index)%FIELDS(n)%field%id, &
                        ci%grids(grid_index)%FIELDS(n)%field%name, &
                        ci%grids(grid_index)%id,         &
                        var_nodims(:), &
                        OASIS_DIR, &
                        var_actual_shape(:), &
                        OASIS_Real, &
                        ierror)

    if (ierror /= 0) then
        success = .false.
        call ci%handle_error("Cannot define OASIS field.", __FILE__, __LINE__)
        return
    endif

    deallocate(var_actual_shape)

    success = .true.

end function