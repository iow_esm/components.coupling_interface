module mod_coupling_interface

use mod_field
use mod_field_1d
use mod_field_2d
use mod_grid
use mod_log
use mod_flexible_arguments, only : flexible_arguments

use iso_fortran_env, only : output_unit, error_unit

implicit none

include 'mpif.h'

private

public :: coupling_interface, MAX_GRIDS



type coupling_interface

    character(:), allocatable :: comp_name
    integer :: comp_id = 0 ! intialize id with default value (should be overwritten by derived class or left unused)
    integer :: local_communicator = MPI_COMM_WORLD ! intialize local_communicator with default value (should be overwritten by derived class)
    integer :: my_pe = 0    ! usually set by the derived class
    integer :: n_pes = 1    ! usually set by the derived class

    type(grid), dimension(:), allocatable :: grids
    integer :: n_grids = 0
    type(logger), allocatable :: log

    

    contains

    procedure :: add_grid
    procedure :: receive_field
    procedure :: send_field

    procedure :: process_input_field
    procedure :: process_output_field

    procedure :: add_input_field_1d
    procedure :: add_input_field_2d
    generic :: add_input_field => add_input_field_1d, add_input_field_2d

    procedure :: add_output_field_1d
    procedure :: add_output_field_2d
    generic :: add_output_field => add_output_field_1d, add_output_field_2d  

    procedure :: complete_initialization 

    procedure :: synchronize_all
    procedure :: synchronize_me
    
    procedure :: handle_error

    procedure :: terminate

    procedure :: get_local_communicator

    final :: destructor
end type coupling_interface

interface coupling_interface
    procedure :: initialize
end interface

integer, parameter :: MAX_GRIDS = 10

contains

! base constructor
function initialize(comp_name, logfile_name, verbosity_level, log_header) result(ci)
    
    type(coupling_interface) :: ci

    character(*)   :: comp_name

    integer, optional :: verbosity_level
    character(*), optional :: logfile_name
    character(*), optional, intent(in) :: log_header

    allocate(character(len(comp_name)) :: ci%comp_name)
    ci%comp_name = comp_name

    ! allocate container for used grids
    allocate(ci%grids(MAX_GRIDS))

    ! initialize logger, if wanted i.e. a log file name is given
    allocate(logger :: ci%log)
    if (.not. present(logfile_name)) return

    ci%log = logger(logfile_name, verbosity_level, log_header)
    call ci%log%write("Initialized basic coupling interface.", LOG_INFO)
    
end function

function add_grid(ci, name, shape, points) result(success)
    logical :: success

    class(coupling_interface) :: ci
    character(*), intent(in) :: name
    integer, dimension(:), intent(in) :: shape
    integer, dimension(:), intent(in), optional :: points

    write(ci%log%buffer,*) "Add grid ", name, " with shape ", shape, " to coupling interface..."
    call ci%log%write(ci%log%buffer, LOG_INFO)

    if (ci%n_grids .ge. MAX_GRIDS) then
        call ci%log%write("Maximum number of grids exceeded.", LOG_ERROR)
        success = .false.
        return
    endif

    ci%n_grids = ci%n_grids + 1
    ci%grids(ci%n_grids) = grid(name, shape, points, log=ci%log)

    call ci%log%write("done.", LOG_INFO)

    success = .true.
    
end function

function add_input_field_1d(ci, grid_index, name, data, process, arguments) result(success)
    logical :: success

    class(coupling_interface) :: ci
    integer :: grid_index
    character(*), intent(in) :: name
    real, dimension(:), intent(in) :: data
    procedure(process_template_1d), optional :: process
    class(flexible_arguments), optional :: arguments

    if (.not. ci%grids(grid_index)%add_input_field_1d(name, data, process, arguments)) then
        call ci%log%write("Cannot add input field to grid.", LOG_ERROR)
        success = .false.
        return
    endif

    write(ci%log%buffer,*) 'Added input field ', ci%grids(grid_index)%n_input_fields, '(', \
    ci%grids(grid_index)%input_fields(ci%grids(grid_index)%n_input_fields)%field%name, ', 1D)',\
    ' on grid ', grid_index, '(', ci%grids(grid_index)%name, ')'
    call ci%log%write(ci%log%buffer, LOG_INFO)   
    
    success = .true.

end function

function add_input_field_2d(ci, grid_index, name, data, process, arguments) result(success)
    logical :: success

    class(coupling_interface) :: ci
    integer :: grid_index
    character(*), intent(in) :: name
    real, dimension(:,:), intent(in) :: data
    procedure(process_template_2d), optional :: process
    class(flexible_arguments), optional :: arguments

    if (.not. ci%grids(grid_index)%add_input_field_2d(name, data, process, arguments)) then
        call ci%log%write("Cannot add input field to grid.", LOG_ERROR)
        success = .false.
        return
    endif

    write(ci%log%buffer,*) 'Added input field ', ci%grids(grid_index)%n_input_fields, '(', \
    ci%grids(grid_index)%input_fields(ci%grids(grid_index)%n_input_fields)%field%name, ', 2D)',\
    ' on grid ', grid_index, '(', ci%grids(grid_index)%name, ')'
    call ci%log%write(ci%log%buffer, LOG_INFO)
    
    success = .true.

end function

function add_output_field_1d(ci, grid_index, name, data, process, arguments) result(success)
    logical :: success

    class(coupling_interface) :: ci
    integer :: grid_index
    character(*), intent(in) :: name
    real, dimension(:), intent(in) :: data
    procedure(process_template_1d), optional :: process
    class(flexible_arguments), optional :: arguments

    if (.not. ci%grids(grid_index)%add_output_field_1d(name, data, process, arguments)) then
        call ci%log%write("Cannot add output field to grid.", LOG_ERROR)
        success = .false.
        return
    endif

    write(ci%log%buffer,*) 'Added output field ', ci%grids(grid_index)%n_output_fields, '(', \
    ci%grids(grid_index)%output_fields(ci%grids(grid_index)%n_output_fields)%field%name, ', 1D)',\
    ' on grid ', grid_index, '(', ci%grids(grid_index)%name, ')'
    call ci%log%write(ci%log%buffer, LOG_INFO)    
    
    success = .true.
    
end function

function add_output_field_2d(ci, grid_index, name, data, process, arguments) result(success)
    logical :: success

    class(coupling_interface) :: ci
    integer :: grid_index
    character(*), intent(in) :: name
    real, dimension(:,:), intent(in) :: data
    procedure(process_template_2d), optional :: process
    class(flexible_arguments), optional :: arguments

    if (.not. ci%grids(grid_index)%add_output_field_2d(name, data, process, arguments)) then
        call ci%log%write("Cannot add output field to grid.", LOG_ERROR)
        success = .false.
        return
    endif
    
    write(ci%log%buffer,*) 'Added output field ', ci%grids(grid_index)%n_output_fields, '(', \
    ci%grids(grid_index)%output_fields(ci%grids(grid_index)%n_output_fields)%field%name, ', 2D)',\
    ' on grid ', grid_index, '(', ci%grids(grid_index)%name, ')'
    call ci%log%write(ci%log%buffer, LOG_INFO)  

    success = .true.
    
end function

function complete_initialization(ci) result(success)
    logical :: success

    class(coupling_interface) :: ci

    call ci%log%write("You are using the empty complete_initialization base class method. Please override this method if your interface needs something special.", LOG_WARNING)
    success = .true.

end function

function synchronize_all(ci) result(success)
    logical :: success

    class(coupling_interface) :: ci

    call ci%log%write("You are using the empty synchronize base class method. Please override this method if your interface needs something special.", LOG_WARNING)
    success = .true.

end function

function synchronize_me(ci) result(success)
    logical :: success

    class(coupling_interface) :: ci

    call ci%log%write("You are using the empty synchronize base class method. Please override this method if your interface needs something special.", LOG_WARNING)
    success = .true.

end function

function receive_field(ci, grid_index, field_index, current_time) result(success)
    logical :: success

    class(coupling_interface) :: ci
    integer, intent(in) :: grid_index, field_index
    integer, intent(in) :: current_time

    call ci%log%write("You are using the empty receive_field base class method. Please override this method.", LOG_WARNING)

    ! do something like
    !select type (f => ci%grid(grid_index)%input_fields(field_index)%field)
        !class is (field_2d)
            !! do your specific routine for receiving 2D field

        !class is (field_1d)
            !! do your specific routine for receiving 1D field
    success = .true.

end function

function send_field(ci, grid_index, field_index, current_time) result(success)
    logical :: success

    class(coupling_interface) :: ci
    integer, intent(in) :: grid_index, field_index
    integer, intent(in) :: current_time

    call ci%log%write("You are using the empty send_field base class method. Please override this method.", LOG_WARNING)

    success = .true.

end function

subroutine process_input_field(ci, grid_index, field_index)
    class(coupling_interface) :: ci
    integer :: grid_index, field_index
    
    select type (f => ci%grids(grid_index)%input_fields(field_index)%field)
    class is (field_1d) 
        call f%process_field()
    class is (field_2d)
        call f%process_field()
    end select

end subroutine

subroutine process_output_field(ci, grid_index, field_index)
    class(coupling_interface) :: ci
    integer :: grid_index, field_index
    
    select type (f => ci%grids(grid_index)%output_fields(field_index)%field)
    class is (field_1d) 
        call f%process_field()
    class is (field_2d)
        call f%process_field()
    end select

end subroutine

function terminate(ci) result(success)
    logical :: success

    class(coupling_interface) :: ci

    call ci%log%write("You are using the empty terminate base class method. Please override this method if your interface needs something special.", LOG_WARNING)
    success = .true.
end function

subroutine handle_error(ci, message, file, line)
    class(coupling_interface) :: ci
    character(*) :: message
    character(len=*), intent(in), optional :: file
    integer, intent(in), optional :: line

    if (.not. ci%log%initialized) then
        call stderr(message, file, line)
        return
    endif

    call ci%log%write(message, LOG_ERROR, file, line)

end subroutine

subroutine destructor(ci)
    type(coupling_interface) :: ci
    integer :: i

    if (allocated(ci%grids)) then
        deallocate(ci%grids)
    endif

end subroutine

function get_local_communicator(ci) result(local_communicator)
    integer :: local_communicator

    class(coupling_interface) :: ci

    local_communicator = ci%local_communicator
    
end function get_local_communicator




end module mod_coupling_interface