! define concatenation macros (cannot be nested, thus for each length needed there is one: stupid)
#define join_(a, b) a ## b
#define join(a, b) join_(a,b)

#define join3_(a, b, c) a ## b ## c 
#define join3(a, b, c) join3_(a, b, c)

#define join4_(a, b, c, d) a ## b ## c ## d
#define join4(a, b, c, d) join4_(a, b, c, d)

! construct needed names from the given direction (DIR = out, in) and dimension (DIM = 1d, 2d; SH = ":", ":,:") 
#define FUNCTION_NAME join4(add_, DIR, put_field_, DIM)
#define PROCESS_TEMPLATE join(process_template_, DIM)
#define FIELD join(field_, DIM)
#define FIELDS join(DIR, put_fields)
#define NFIELDS join3(n_, DIR, put_fields)
#define MAX_FIELDS join3(MAX_, DIR, PUT_FIELDS)

function FUNCTION_NAME(g, name, data, process, arguments) result(success)
    logical :: success

    class(grid) :: g
    character(*), intent(in) :: name
    real, dimension(SH), intent(in) :: data
    procedure(PROCESS_TEMPLATE), optional :: process
    class(flexible_arguments), optional :: arguments

    if (g%NFIELDS .eq. MAX_FIELDS) then
        if(allocated(g%log)) call g%log%write("Maximal number of fields reached. Cannot add more, please increase maximal number of fields!", LOG_ERROR, __FILE__, __LINE__) 
        success = .false.
        return
    endif

    if (.not. all(g%shape .eq. shape(data))) then
        if(allocated(g%log)) call g%log%write("Shape of field does not match grid shape.", LOG_ERROR, __FILE__, __LINE__)
        success = .false.
        return
    endif    

    g%NFIELDS = g%NFIELDS+ 1

    allocate(FIELD::g%FIELDS(g%NFIELDS)%field)
    g%FIELDS(g%NFIELDS)%field = FIELD(name, data, process, arguments)

    success = .true.
    
end function