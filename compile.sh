OASIS3_LIB="${IOW_ESM_ROOT}/components/OASIS3-MCT/oasis3-mct/IOW_ESM_${configuration}"


INCLUDES="-I${OASIS3_LIB}/build/lib/mct \
          -I${OASIS3_LIB}/build/lib/psmile.MPI1 \
          -I${OASIS3_LIB}/build/lib/psmile.MPI1 \
          -I${OASIS3_LIB}/build/lib/mct \
          -I${IOW_ESM_NETCDF_INCLUDE}"

LIBS="${OASIS3_LIB}/lib/*.a \
      ${IOW_ESM_NETCDF_LIBRARY}/*.a \
      ${IOW_ESM_NETCDF_LIBRARY}/*.so "    

build_dir="build_${configuration}"
if [ "$rebuild" == "rebuild" ]; then
	rm -rf "${build_dir}"
fi
mkdir -p ./"${build_dir}"

cd "${build_dir}"

# basic library
$FC -c $FFLAGS ../src/flexible_arguments.F90
$FC -c $FFLAGS ../src/log.F90
$FC -c $FFLAGS ../src/field.F90
$FC -o field_1d.o -c $FFLAGS -DDIM=1 ../src/field_nd.F90
$FC -o field_2d.o -c $FFLAGS -DDIM=2 ../src/field_nd.F90
$FC -c $FFLAGS ../src/grid.F90 $INCLUDES
$FC -c $FFLAGS ../src/coupling_interface.F90
$FC -c $FFLAGS ../src/action.F90
$FC -c $FFLAGS ../src/coupling_cycle.F90

# specific libraries
$FC -c $FFLAGS ../src/oasis_coupling_interface/oasis_coupling_interface.F90 $INCLUDES 

library="libcoupling_interface.a" 

if [ -f ${library} ]; then
    rm ${library}
fi
$AR rcv ${library} *.o

$AR cqT combined.a ${library} $LIBS  && echo -e 'create combined.a\naddlib combined.a\nsave\nend' | $AR -M

mv combined.a ${library}

cd -