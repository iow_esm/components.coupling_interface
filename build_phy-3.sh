#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module load intel/2022.2/compiler/2022.1.0
module load intel/2022.2/mpi/2021.6.0
module load netcdf/4.9.2-intel_2022.2

# search for netcdf-fortran paths in environment variables
export IOW_ESM_NETCDF_INCLUDE="/sw/data/netcdf/OS_15.5/x86_64-suse-linux/4.9.2/intel_2022/include"
export IOW_ESM_NETCDF_LIBRARY="/sw/data/netcdf/OS_15.5/x86_64-suse-linux/4.9.2/intel_2022/lib64"

FC=mpiifort
AR="ar"
IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

if [ $debug == "debug" ]; then
	FFLAGS="-O0 -r8 -fp-model precise -xHost -g -traceback -check all -DIOW_ESM_DEBUG"
	configuration="DEBUG"
else
	FFLAGS="-O3 -r8 -no-prec-div -fp-model fast "
	configuration="PRODUCTION"
fi

source ./compile.sh


