#!/bin/bash

debug=${1:-"release"}
rebuild=${2:-"fast"}

module load intel-oneapi-compilers/2023.2.1
module load intel-oneapi-mpi/2021.10.0
module load netcdf-fortran/4.6.1-mpi

FC=mpiifort
AR="ar"
IOW_ESM_ROOT="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )/../.."

# SET SYSTEM-SPECIFIC COMPILER OPTIONS AND PATHS
# include paths
export IOW_ESM_NETCDF_INCLUDE=`env | grep ^INCLUDE= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf-fortran"){print $i}}}'`
export IOW_ESM_NETCDF_LIBRARY=`env | grep ^LIBRARY_PATH= | tr '=' ' ' | tr ':' ' ' | awk '{for(i=1; i<NF; i++){if($i~"netcdf-fortran"){print $i}}}'`

if [ $debug == "debug" ]; then
	FFLAGS="-O0 -r8 -fp-model precise -xHost -g -traceback -check all -DIOW_ESM_DEBUG"
	configuration="DEBUG"
else
	FFLAGS="-O3 -r8 -no-prec-div -fp-model fast -xCORE-AVX512 "
	configuration="PRODUCTION"
fi

source ./compile.sh


